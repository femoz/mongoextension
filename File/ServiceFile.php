<?php
/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo;

/**
 *  
 * @author Martin Charouzek <martin@charouzkovi.info>
 * 
 */
class ServiceFile extends \Nette\Object {

    /**
     *
     * @var \Machar\Mongo\MongoClient
     */
    protected $mongo;
    /**
     *
     * @var string $table
     */
    protected $table;

    public function __construct( $mongo, $table) {
        $this->mongo = $mongo;
        $this->table = $table;
    }

    /**
     * 
     * @param string $idMongo
     * @param int $width
     * @param int $height
     * @return \File|null
     */
    public function get($idMongo) {
            return $this->mongo->select($this->table)->where(array("_id" => new \MongoId($mongoId)))->fetch();

    }

    /**
     * 
     * @param File $file
     * @return string $mongoId
     */
    public function update(File $file) {
        $list = array();
        $list['source'] = $file->getSource();
        $list['insertDate'] = $file->getInsertDate();
        return $this->mongo->update($this->table, array("_id" => new \MongoId($file->getIdMongo())),$list)->exec();
    }

    /**
     * 
     * @param File $file
     * @return string $mongoId
     */
    public function save(File $file, array $args = array()) {
        $list = array();
        $list['source'] = $file->getSource();
        $list['insertDate'] = $file->getInsertDate();
        foreach ($args as $key => $value) {
            $list[$key] = $value;
        }
        return $this->mongo->insert($this->table, $list)->exec();
    }
    
    /**
     * 
     * @param string $idMongo
     * @return string $idMongo
     */
    public function delete($idMongo) {
        return $this->mongo->delete($this->table)->where(array("_id" => new \MongoId($idMongo)))->exec();
    }

}

?>
