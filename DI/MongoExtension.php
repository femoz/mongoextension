<?php

/**
 * This file is part of the Machar libs (http://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo\DI;

use Machar;
use Nette;
use Nette\Config\Configurator;
use Nette\Config\Compiler;
use Nette\DI\ContainerBuilder;
use Nette\DI\Statement;
use Nette\Utils\Validators;

/**
 * @author Martin Charouzek <martin@charouzkovi.info>
 */
class MongoExtension extends Nette\Config\CompilerExtension {

    /**
     * @var array
     */
    public $defaults = array("master" => array(
            'host' => 'localhost',
            'port' => 27017,
            'database' => 0
        )
    );

    public function loadConfiguration() {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        $client = $builder->addDefinition($this->prefix('client'))
                ->setClass('Machar\Mongo\MongoClient', array(
            'host' => $config['master']['host'],
            'port' => $config['master']['port'],
            'database' => $config['master']['database'],
                ));

        $builder->addDefinition($this->prefix('file'))->setClass('Machar\Mongo\ServiceFile',array(
            '@mongo.client','file'
        ));

        $builder->addDefinition($this->prefix('picture'))->setClass('Machar\Mongo\ServicePicture',array(
            '@mongo.client','photos'
        ));

        if ($builder->parameters['debugMode']) {
            $client->addSetup('setPanel');
        }

        $builder->addDefinition($this->prefix('panel'))
                ->setFactory('Machar\Mongo\Diagnostics\Panel::register');
    }

    /**
     * @param \Nette\Config\Configurator $config
     */
    public static function register(Configurator $config) {
        $config->onCompile[] = function (Configurator $config, Compiler $compiler) {
                    $compiler->addExtension('mongo', new MongoExtension());
                };
    }

}
