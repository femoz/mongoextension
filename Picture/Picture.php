<?php

/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */
namespace Machar\Mongo;
/**
 *  
 * @author Martin Charouzek <martin@charouzkovi.info>
 * 
 */
class Picture extends \Machar\Mongo\File {

    /**
     *
     * @var int $width
     */
    protected $width;
    /**
     *
     * @var int $height
     */
    protected $height;    
    
    /**
     * 
     * @param type $resource
     * @return \static Picture
     */
    public static function fromString($resource) {
        $type = \Nette\Utils\MimeTypeDetector::fromString($resource);
        $file = new static;
        $file->setSource(base64_encode($resource));
        $file->setInsertDate(time());
        if (\Nette\Utils\Strings::startsWith($type, "image")) {
            $im = \Nette\Image::fromString($resource);
            $file->setWidth($im->getWidth());
            $file->setHeight($im->getHeight());
        }        
        return $file;
    }
    

    public function getWidth() {
        return $this->width;
    }

    public function setWidth($width) {
        $this->width = $width;
    }

    public function getHeight() {
        return $this->height;
    }

    public function setHeight($height) {
        $this->height = $height;
    }


}

?>
